import React from 'react';
import {
    StatusBar,
    StyleSheet,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from './src/pages/Home';
import Splash from './src/pages/Splash';
import Escada from './src/pages/Escada';
import Esquadro from './src/pages/Esquadro';
import Telhas from './src/pages/Telhas';
import Tijolos from './src/pages/Tijolos';


const Stack = createNativeStackNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <StatusBar backgroundColor="#0c7e7e" />
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false, title: 'Engenharia Fácil' }} />
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false, title: 'Engenharia Fácil' }} />
                <Stack.Screen name="Escada" component={Escada} options={{headerShown:true,title:'Escada Reta',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#023047"},headerTitleStyle: {color:"#FFFFFF"}}} />
                <Stack.Screen name="Esquadro" component={Esquadro} options={{headerShown:true,title:'Esquadro',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#023047"},headerTitleStyle: {color:"#FFFFFF"}}} />
                <Stack.Screen name="Telhas" component={Telhas} options={{headerShown:true,title:'Telhas',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#023047"},headerTitleStyle: {color:"#FFFFFF"}}} />
                <Stack.Screen name="Tijolos" component={Tijolos} options={{headerShown:true,title:'Tijolos',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#023047"},headerTitleStyle: {color:"#FFFFFF"}}} />
            </Stack.Navigator>
            
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
