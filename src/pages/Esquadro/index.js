import { LinearGradient } from 'expo-linear-gradient';
import React, { useEffect, useState, useRef } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Keyboard,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet
} from 'react-native';

export default function Esquadro({ navigation }) {

    const [ladoA, setLadoA] = useState('');
    const [ladoB, setLadoB] = useState('');

    const [larguraDiagonal, setLarguraDiagonal] = useState(0);

    const [resultado, setResultado] = useState(0);

    useEffect(()=>{
        setResultado(0);
    },[ladoA,ladoB]);


    const calcular = () => {
        let pode_calcular = false;
        let hipotenusa = 0;
        let quadrados = 0;

        if(ladoA > 0 && ladoB > 0){
            pode_calcular =  true;
        }

        if (pode_calcular) {
            quadrados = (ladoA*ladoA) + (ladoB*ladoB);
            hipotenusa = Math.sqrt(quadrados)

            setLarguraDiagonal(hipotenusa.toFixed(2)*1);
            setResultado(1);
            Keyboard.dismiss();
        } else {
            alert("Preencha os campos primeiro");
        }
    };


    return (

        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="rgb(2, 48, 71)" />
            <LinearGradient
                style={styles.gradient}

                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}
                locations={[.2, 0.9]}
                colors={['#9c63af', '#023047']
                }
            >
            <ScrollView style={styles.scrollView}>
                <Text numberOfLines={2} style={styles.title}>Cálculo para esquadro</Text>

                <View style={styles.row}>
                    <View style={styles.viewRow}>
                        <Text style={styles.titlePicker}>(Metros)</Text>
                        <TextInput
                            style={styles.inputRow}
                            placeholder='Lado A'
                            value={ladoA}
                            onChangeText={t => setLadoA(t)}
                            keyboardType="numeric"
                            placeholderTextColor='#FFFFFF'
                        />
                    </View>
                </View>

                <View style={styles.row}>
                    <View style={styles.viewRow}>
                        <Text style={styles.titlePicker}>(Metros)</Text>
                        <TextInput
                            style={styles.inputRow}
                            placeholder='Lado B'
                            value={ladoB}
                            onChangeText={t => setLadoB(t)}
                            keyboardType="numeric"
                            placeholderTextColor='#FFFFFF'
                        />
                    </View>
                </View>
               

                <View style={styles.row}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={calcular}
                    >
                        <Text style={styles.text}>Calcular</Text>
                    </TouchableOpacity>
                </View>

                <View style={[styles.rowResultado]}>
                    {resultado != 0 &&
                        <>
                            <Text style={styles.titleResultado}>Resultado</Text>
                            <View style={styles.triangulo}>
                                <Text style={[styles.lados,styles.ladoA]}>A: {ladoA == '' ? 0 : ladoA} M</Text>
                                <Text style={[styles.lados,styles.diagonal]}>X: {larguraDiagonal} M</Text>
                                <Text style={[styles.lados,styles.ladoB]}>B: {ladoB == '' ? 0 : ladoB} M</Text>
                            </View>
                            <View style={styles.info} >
                                <Text style={styles.msg}>O lado X é a distância correta entre uma extremidade e outra.</Text>
                            </View>
                        </>
                    }
                </View>
            </ScrollView>
            </LinearGradient>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '100%',
        maxHeight: '100%',
        padding: 15
    },
    input: {
        height: 48,
        borderWidth: 1,
        borderColor: 'gray',
        padding: 10,
        color: '#FFFFFF'
    },
    inputRow: {
        height: 48,
        color: '#FFFFFF',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingStart: 10,
    },
    viewRow: {
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 20
    },
    title: {
        fontSize: 25,
        marginBottom: 20,
        marginTop: 20,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        width: '100%',
    },
    titleResultado: {
        fontSize: 25,
        alignSelf: 'center',
        marginBottom: 10,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: 'gray',
        width: '100%',
        borderRadius: 20
    },
    rowResultado: {
        flexDirection: 'column',
        alignSelf: 'center',
        padding: 5,
        width: '100%',
        alignItems: 'center',
    },
    text: {
        fontSize: 25,
        color: 'white'
    },
    titlePicker: {
        marginTop: -12,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: -8,
        backgroundColor: 'rgb(48,63,102)',
        marginRight: 'auto',
        marginLeft: 5,
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold',
        zIndex: 9,
        borderRadius: 50
    },
    triangulo: {
        marginTop: 30,
        marginBottom: 30,
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderRightWidth: 150,
        borderBottomWidth: 100,
        borderLeftWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'red',
        borderLeftColor: 'transparent',
    },
    lados: {
        width: 100,
        color: '#FFF',
    },
    ladoA: {
        marginLeft: -30,
        marginTop: 50,
        
    },
    ladoB: {
        marginTop: 30,
        marginLeft: 40
    },
    diagonal: {
        marginTop: -17,
        marginLeft: 60
    },
    info: {
       display: 'flex',
       width: '100%',
    },
    msg: {
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20,
        color: '#FFF',
        textAlign: 'center',
        fontSize: 20
    }
});
