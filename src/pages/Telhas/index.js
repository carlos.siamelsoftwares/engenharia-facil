import React, { useEffect, useState, useRef } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Keyboard,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Picker } from '@react-native-picker/picker';


export default function Telhas({ navigation }) {

    function calculo(opcao) {
        if (opcao == 'escada')
            navigation.navigate("Escada", { opcao });
        if (opcao == 'esquadro')
            navigation.navigate("Esquadro", { opcao });
    }

    function sair() {
        navigation.navigate("Home");
    }

    const [tipoTelha, setTipoTelha] = useState('12.5');
    const [tipoTelhado, setTipoTelhado] = useState(1);
    const [inclinacao, setInclinacao] = useState('1.044');
    const [largura, setLargura] = useState('');
    const [comprimento, setComprimento] = useState('');
    const [percentual, setPercentual] = useState('');
    const [resultado, setResultado] = useState(0);



    const calcular = () => {
        let pode_calcular = true;
        let area =  Number(largura) * Number(comprimento);
        let res = 0;
        let percent = percentual == '' ? 0 : percentual;

        if(largura.length == 0 || comprimento.length == 0 || percentual.length == 0){
            pode_calcular = false;
        }

        if (pode_calcular) {
            res = area * Number(inclinacao) * Number(tipoTelha);

            if(tipoTelhado == 2){
                res = res * 2;
            }

            res = res + (percent/100)*res;
            
            res = Math.round(res)
            setResultado(res);
            Keyboard.dismiss();
        } else {
            alert("Preencha os campos primeiro");
        }
    };


    return (

        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="rgb(2, 48, 71)" />
            <LinearGradient
                style={styles.gradient}

                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}
                locations={[.2, 0.9]}
                colors={['#9c63af', '#023047']
                }
            >
            <ScrollView style={styles.scrollView}>
                <Text numberOfLines={2} style={styles.title}>Cálculo de Telhas</Text>
                    <>
                    <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Tipo de Telhado)</Text>
                                <Picker
                                    mode='dialog'
                                    dropdownIconColor='white'
                                    prompt='Escolha um tipo de telhado'
                                    selectedValue={tipoTelhado}
                                    style={styles.picker}
                                    onValueChange={(itemValue, itemIndex) =>
                                        setTipoTelhado(itemValue)
                                    }>
                                    <Picker.Item label="Uma água" value="1" />
                                    <Picker.Item label="Duas águas" value="2" />
                                </Picker>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Tipo de telha)</Text>
                                <Picker
                                    mode='dialog'
                                    dropdownIconColor='white'
                                    prompt='Escolha um tipo de telha'
                                    selectedValue={tipoTelha}
                                    style={styles.picker}
                                    onValueChange={(itemValue, itemIndex) =>
                                        setTipoTelha(itemValue)
                                    }>
                                    <Picker.Item label="Telha americana (12,5 telhas por m²)" value="12.5" />
                                    <Picker.Item label="Telha colonial pequena (24 telhas por m²)" value="24" />
                                    <Picker.Item label="Telha colonial grande (16 telhas por m²)" value="16" />
                                    <Picker.Item label="Telha francesa (16 telhas por m²)" value="16" />
                                    <Picker.Item label="Telha romana (16 telhas por m²)" value="16" />
                                    <Picker.Item label="Telha italiana (14 telhas por m²)" value="14" />
                                    <Picker.Item label="Telha portuguesa (17 telhas por m²)" value="17" />
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.row}>
                           
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Inclinação %)</Text>
                                <Picker
                                    mode='dialog'
                                    dropdownIconColor='white'
                                    prompt='Escolha uma inclinação'
                                    selectedValue={inclinacao}
                                    style={styles.picker}
                                    onValueChange={(itemValue, itemIndex) =>
                                        setInclinacao(itemValue)
                                    }>
                                    <Picker.Item label="5%" value="1.001" />
                                    <Picker.Item label="10%" value="1.005" />
                                    <Picker.Item label="15%" value="1.011" />
                                    <Picker.Item label="20%" value="1.020" />
                                    <Picker.Item label="25%" value="1.031" />
                                    <Picker.Item label="30% (mais utilizado)" value="1.044" />
                                    <Picker.Item label="35%" value="1.059" />
                                    <Picker.Item label="40%" value="1.077" />
                                    <Picker.Item label="45%" value="1.097" />
                                    <Picker.Item label="50%" value="1.118" />
                                    <Picker.Item label="55%" value="1.141" />
                                    <Picker.Item label="60%" value="1.166" />
                                    <Picker.Item label="65%" value="1.193" />
                                    <Picker.Item label="70%" value="1.221" />
                                    <Picker.Item label="75%" value="1.250" />
                                    <Picker.Item label="80%" value="1.281" />
                                    <Picker.Item label="85%" value="1.312" />
                                    <Picker.Item label="90%" value="1.345" />
                                    <Picker.Item label="95%" value="1.379" />
                                    <Picker.Item label="100%" value="1.414" />
                                    
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Largura - com beiral)</Text>
                                <TextInput
                                    style={styles.inputRow}
                                    placeholder='Largura em metros (com beiral)'
                                    value={largura}
                                    onChangeText={t => setLargura(t)}
                                    keyboardType="numeric"
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Comprimento - com beiral)</Text>
                                <TextInput
                                    style={styles.inputRow}
                                    placeholder='Comprimento em metros (com beiral)'
                                    value={comprimento}
                                    onChangeText={t => setComprimento(t)}
                                    keyboardType="numeric"
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Percentual de quebra %)</Text>
                                <TextInput
                                    style={styles.inputRow}
                                    placeholder='Percentual de quebra'
                                    value={percentual}
                                    onChangeText={t => setPercentual(t)}
                                    keyboardType="numeric"
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                        </View>
                    </>
               

                <View style={styles.row}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={calcular}
                    >
                        <Text style={styles.text}>Calcular</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.rowResultado}>
                    {resultado != 0 &&
                        <>
                            <Text style={styles.titleResultado}>RESULTADO</Text>
                            <Text style={styles.labelResultado}>TELHAS NECESSÁRIAS: {resultado}</Text>
                        </>
                    }
                </View>
            </ScrollView>
            </LinearGradient>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '100%',
        maxHeight: '100%',
        padding: 15
    },
    input: {
        height: 48,
        borderWidth: 1,
        borderColor: 'gray',
        padding: 10,
        color: '#FFFFFF'
    },
    inputRow: {
        height: 48,
        color: '#FFFFFF',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingStart: 10,
    },
    viewRow: {
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 20
    },
    title: {
        fontSize: 25,
        marginBottom: 20,
        marginTop: 20,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        width: '100%',
    },
    labelResultado: {
        fontSize: 18,
        marginBottom: 10,
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    titleResultado: {
        fontSize: 25,
        alignSelf: 'center',
        marginBottom: 10,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    viewInput: {
        width: '100%',
        padding: 5,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: 'gray',
        width: '100%',
        borderRadius: 20
    },
    rowResultado: {
        flex: 1,
        flexDirection: 'column',
        alignSelf: 'center',
        padding: 5,
        width: '100%'
    },
    text: {
        fontSize: 25,
        color: 'white'
    },
    picker: {
        color: '#FFF'
    },
    titlePicker: {
        marginTop: -12,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: -8,
        backgroundColor: 'rgb(48,63,102)',
        marginRight: 'auto',
        marginLeft: 5,
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold',
        zIndex: 9,
        borderRadius: 50
    }
});
