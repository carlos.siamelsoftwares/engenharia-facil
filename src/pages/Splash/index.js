import React, { useEffect, useState, useRef, } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    Image,
    StyleSheet,
    StatusBar
} from 'react-native';


export default function Splash({ navigation }) {

    setTimeout(()=>{
        navigation.reset({
            routes: [{name: 'Home'}]
        });
    },3000);

    return (

        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="rgb(2, 48, 71)" />
            <Text style={styles.title}>Sua obra mais fácil</Text>

            <View style={styles.instituicao}>
                <Image source={require("../../../assets/logo_.png")} style={styles.logo} />
                {/* <Text style={styles.textInstituicaco}>Alfa Unipac</Text> */}
            </View>
            <View style={styles.footer}>
                <Text style={styles.textFooterBold}>Feito por Gustavo Ribeiro Pereira</Text>
                <Text style={styles.textFooterBold}>&amp;</Text>
                <Text style={styles.textFooterBold}>Pedro Giovani Miranda Pena Neto</Text>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#023047',
    },
    logo: {
        width: 280,
        height: 50,
        resizeMode: 'stretch',
    },
    title: {
        color: 'rgb(216, 223, 232)',
        fontWeight:'bold',
        fontSize: 30
    },
    textFooter: {
        color: '#CCCCCC'
    },
    textFooterBold: {
        color: 'rgb(216, 223, 232)',
        fontWeight: 'bold',
        fontSize: 12,
        marginBottom: 5
    },
    instituicao: {
        display: 'flex',
        alignItems: 'center'
    },
    footer: {
        alignItems: 'center',
        height: 0,
    },
    textInstituicaco: {
        color: 'rgb(216, 223, 232)',
        fontWeight: 'bold',
        fontSize: 25,
        marginBottom: 5
    }
});
