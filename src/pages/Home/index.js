import React from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    SafeAreaView,
    Image,
    StatusBar,
    StyleSheet
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';


export default function Home({ navigation }) {

    function calculo(opcao) {
        if (opcao == 'escada')
            navigation.navigate("Escada", { opcao });
        if (opcao == 'esquadro')
            navigation.navigate("Esquadro", { opcao });
        if (opcao == 'telhas')
            navigation.navigate("Telhas", { opcao });
        if (opcao == 'tijolos')
            navigation.navigate("Tijolos", { opcao });
    }

    return (

        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="rgb(2, 48, 71)" />
            <LinearGradient
                style={styles.gradient}

                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}
                locations={[.2, 0.9]}
                colors={['#9c63af', '#023047']
                }
            >


                <View style={styles.itens}>
                    <View>
                        <Text style={{ fontSize: 40, alignSelf: 'center', marginBottom: 30, color: '#FFFFFF' }}>Engenharia Fácil</Text>
                        <Image source={require('../../../assets/logo.png')} style={styles.logo} />
                    </View>

                    <View style={styles.botoes}>
                        {/* <Text style={styles.title}>Selecione uma Opção</Text> */}
                        <TouchableOpacity
                            style={styles.itemBotao}
                            onPress={() => calculo('escada')}
                        >
                            <Text style={styles.text}>Escada Reta</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.itemBotao}
                            onPress={() => calculo('esquadro')}
                        >
                            <Text style={styles.text}>Esquadro</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.botoes}>
                        <TouchableOpacity
                            style={styles.itemBotao}
                            onPress={() => calculo('telhas')}
                        >
                            <Text style={styles.text}>Telhas</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.itemBotao}
                            onPress={() => calculo('tijolos')}
                        >
                            <Text style={styles.text}>Tijolos</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '100%',
        maxHeight: '100%',
    },
    itemBotao: {
        width: 100,
        height: 100,
        backgroundColor: '#3a3944',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    itens: {
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%',
    },
    text: {
        fontSize: 15,
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    title: {
        fontSize: 25,
        alignSelf: 'center',
        marginBottom: 15,
        color: "#fff",
    },
    logo: {
        width: 150, 
        height: 150, 
        alignSelf: 'center',
        marginBottom: '20%',
        borderRadius: 15
    },
    botoes: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 10,
        justifyContent: 'space-evenly'
    }
});