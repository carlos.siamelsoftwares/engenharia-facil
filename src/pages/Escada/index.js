import React, { useEffect, useState, useRef } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Keyboard,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Picker } from '@react-native-picker/picker';


export default function Escada({ navigation }) {

    function calculo(opcao) {
        if (opcao == 'escada')
            navigation.navigate("Escada", { opcao });
        if (opcao == 'esquadro')
            navigation.navigate("Esquadro", { opcao });
    }

    function sair() {
        navigation.navigate("Home");
    }

    const [larguraDegrau, setLarguraDegrau] = useState('');
    const [peDireito, setPeDireito] = useState('');

    const [alturaEspelho, setAlturaEspelho] = useState(0);

    const [qtdeEspelho, setQtdeEspelho] = useState(0);

    const [espacoOcupado, setEspacoOcupado] = useState(0);

    const [resultado, setResultado] = useState(0);



    const calcular = () => {
        let pode_calcular = false;
        let espelho = 0.00;
        let qtde_espelho = 0;
        let espaco_ocupado = 0.00;

        if(larguraDegrau > 0 && peDireito > 0){
            pode_calcular = true;
        }

        if (pode_calcular) {

            let pd = peDireito * 100;

            espelho =  (Math.round((64 - parseFloat(larguraDegrau)) / 2) / 100) * 100;
            qtde_espelho = (Math.round(parseInt(pd)/espelho) / 100) * 100;
            espaco_ocupado =  (Math.round((qtde_espelho -1) * parseFloat(larguraDegrau) / 100)/ 100 ) * 100;

            setAlturaEspelho(espelho.toFixed(2)*1);
            setQtdeEspelho(qtde_espelho.toFixed(2)*1);
            setEspacoOcupado(espaco_ocupado.toFixed(2)*1);


            setResultado(1);
            Keyboard.dismiss();
        } else {
            alert("Preencha os campos primeiro");
        }
    };


    return (

        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="rgb(2, 48, 71)" />
            <LinearGradient
                style={styles.gradient}

                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}
                locations={[.2, 0.9]}
                colors={['#9c63af', '#023047']
                }
            >
            <ScrollView style={styles.scrollView}>
                <Text numberOfLines={2} style={styles.title}>Cálculo de Escada reta</Text>

                {/* {(area == 'retangulo') && */}

                    <>
                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Centímetros)</Text>
                                <TextInput
                                    style={styles.inputRow}
                                    placeholder='Largura do degrau'
                                    value={larguraDegrau}
                                    onChangeText={t => setLarguraDegrau(t)}
                                    keyboardType="numeric"
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.viewRow}>
                                <Text style={styles.titlePicker}>(Metros)</Text>
                                <TextInput
                                    style={styles.inputRow}
                                    placeholder='Pé direito'
                                    value={peDireito}
                                    onChangeText={t => setPeDireito(t)}
                                    keyboardType="numeric"
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                        </View>
                    </>
                {/* } */}
               

                <View style={styles.row}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={calcular}
                    >
                        <Text style={styles.text}>Calcular</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.rowResultado}>
                    {resultado != 0 &&
                        <>
                            <Text style={styles.titleResultado}>RESULTADO</Text>
                            <Text style={styles.labelResultado}>ALTURA DO ESPELHO: {alturaEspelho} CENTÍMETROS</Text>
                            <Text style={styles.labelResultado}>LARGURA DO PISO: {larguraDegrau} CENTÍMETROS</Text>
                            <Text style={styles.labelResultado}>QUANTIDADE DE ESPELHOS: {qtdeEspelho}</Text>
                            <Text style={styles.labelResultado}>ESPAÇO OCUPADO: {espacoOcupado} METROS</Text>
                        </>
                    }
                </View>
            </ScrollView>
            </LinearGradient>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '100%',
        maxHeight: '100%',
        padding: 15
    },
    input: {
        height: 48,
        borderWidth: 1,
        borderColor: 'gray',
        padding: 10,
        color: '#FFFFFF'
    },
    inputRow: {
        height: 48,
        color: '#FFFFFF',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingStart: 10,
    },
    viewRow: {
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 20
    },
    title: {
        fontSize: 25,
        marginBottom: 20,
        marginTop: 20,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        width: '100%',
    },
    labelResultado: {
        fontSize: 18,
        marginBottom: 10,
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    titleResultado: {
        fontSize: 25,
        alignSelf: 'center',
        marginBottom: 10,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    viewInput: {
        width: '100%',
        padding: 5,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: 'gray',
        width: '100%',
        borderRadius: 20
    },
    rowResultado: {
        flex: 1,
        flexDirection: 'column',
        alignSelf: 'center',
        padding: 5,
        width: '100%'
    },
    text: {
        fontSize: 25,
        color: 'white'
    },
    titlePicker: {
        marginTop: -12,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: -8,
        backgroundColor: 'rgb(48,63,102)',
        marginRight: 'auto',
        marginLeft: 5,
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold',
        zIndex: 9,
        borderRadius: 50
    }
});
