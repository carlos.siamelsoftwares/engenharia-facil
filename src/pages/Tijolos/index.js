import React, {useEffect, useState} from 'react';
import {
        Text,
        View,
        TextInput,
        TouchableOpacity,
        Keyboard,
        SafeAreaView,
        ScrollView,
        StatusBar,
        StyleSheet
    } from 'react-native';

import {Picker} from '@react-native-picker/picker';
import { LinearGradient } from 'expo-linear-gradient';


export default function Tijolos( {route,navigation} ){

    const [resultado, setResultado] = useState(0);

    const [areaParede,setAreaParede] = useState('');
    const [areaTijolo,setAreaTijolo] = useState('0.0266');
    const [areaItens,setAreaItens] = useState('');

  
    const calcular = ()=>{ 
        if(areaParede.length == 0 || areaTijolo.length == 0){
            alert("Preencha os camos por favor");
        }else{
            let area = areaParede - areaItens;
            let resultado = "";
            if(area > 0){
                resultado = Math.ceil(area/areaTijolo);
                setResultado(resultado);
                Keyboard.dismiss();
            }else{
               alert("Impossível calcular.")
            }
        }
    };

  useEffect(()=>{
    setResultado('');
  },[areaParede,areaItens,areaTijolo]);


  return (

    <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="rgb(2, 48, 71)"/>
        <LinearGradient
                style={styles.gradient}

                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}
                locations={[.2, 0.9]}
                colors={['#9c63af', '#023047']
                }
            >
        <ScrollView>
            <View>
                <Text style={styles.title}>Cálculo de Tijolos</Text>
            </View>

            <View style={styles.row}>
                <View style={styles.viewRow}>
                    <Text style={styles.titlePicker}>Area da parede(m²)</Text>
                    <TextInput 
                        style={styles.inputRow} 
                        placeholder='Area da parede' 
                        value={areaParede}
                        onChangeText={t=>setAreaParede(t)}
                        keyboardType="numeric"
                        placeholderTextColor = '#FFFFFF'
                    />  
                </View>
            </View>


            <View style={styles.row}>
                <View style={styles.viewRow}>
                    <Text style={styles.titlePicker}> Area de janelas, portas etc.(m²) </Text>
                    <TextInput 
                        style={styles.inputRow} 
                        placeholder='Area de janelas, portas etc.' 
                        value={areaItens}
                        onChangeText={t=>setAreaItens(t)}
                        keyboardType="numeric"
                        placeholderTextColor = '#FFFFFF'
                    />  
                </View>
            </View>

            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View style={styles.pickerContent}>
                        <View>
                            <Text style={styles.titlePicker}>Tipo de tijolo</Text>
                        </View>
                        <Picker
                            mode='dialog'
                            dropdownIconColor='white'
                            prompt='Escolha um tipo de tijolo'
                            style={styles.picker}
                            selectedValue={areaTijolo}
                            onValueChange={(itemValue, itemIndex) =>
                                setAreaTijolo(itemValue)
                            }>
                            <Picker.Item label="6 FUROS (0,14 x 0,19)" value="0.0266" />
                            <Picker.Item label="8 FUROS (0,19 x 0,19)" value="0.0361" />
                            <Picker.Item label="8 FUROS (0,19 x 0,29)" value="0.0551" />
                            <Picker.Item label="9 FUROS (0,14 x 0,24)" value="0.0336" />
                            <Picker.Item label="INTEIRIÇO (0,09 X 0,19)" value="0.0171" />
                            <Picker.Item label="VAZADO (0,07 x 0,25)" value="0.0175" />
                            <Picker.Item label="ECOLÓGICO (0,625 x 0,25)" value="0.15625" />
                            <Picker.Item label="REFRATÁRIO (0,115 x 0,23)" value="0.02645" />
                            <Picker.Item label="CONCRETO (0,19 x 0,39)" value="0.0741" />
                            <Picker.Item label="VIDRO (0,19 x 0,19)" value="0.0361" />
                            <Picker.Item label="CANALETA CERÂMICA (0,14 x 0,24)" value="0.0336" />
                            <Picker.Item label="LAMINADO 21 FUROS (0,115 x 0,245)" value="0.028175" />
                        </Picker>
                    </View>
                </View>
            </View>

            <View style={styles.row}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={calcular}
                >
                    <Text style={styles.text}>Calcular</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.rowResultado}>
                { resultado != 0 &&
                    <>
                    <Text style={styles.titleResultado}>Resultado</Text>
                    <Text style={styles.labelResultado}>A area informada gastará {resultado} tijolos.</Text>
                    </>
                }
            </View>
        </ScrollView>   
        </LinearGradient> 
    </SafeAreaView>
   
  );

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '100%',
        maxHeight: '100%',
        padding: 10
    },
    picker: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
    },
    pickerComLegenda: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
        position: "relative",
        left: -45 
    },
    pickerModelo: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
        position: "relative",
        left: -65 
    },
    pickerContent: {
        flexDirection:'column',width:'100%'
    },
    input: {
        height: 48, 
        borderWidth: 1,
        borderColor: 'gray',  
        padding: 10,
        color: '#FFFFFF'
    },
    inputComPiker: {
        height: 48, 
        color: '#FFFFFF',
        paddingLeft: 4,
        width: '100%',
        justifyContent:'center',
        alignItems:'center',
        paddingStart: 10,
    },
    title: {
        fontSize: 25,
        marginBottom: 20,
        color: '#FFFFFF',
        textAlign:'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        width: '100%',
    },
    inputRow: {
        height: 48,
        color: '#FFFFFF',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingStart: 10,
    },
    label: {
        fontSize: 17,
        marginBottom: 10,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    labelResultado: {
        fontSize: 18,
        marginBottom: 10,
        alignSelf: 'flex-start',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    titleResultado: {
        fontSize: 25,
        marginBottom: 10,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    viewPicker: {
       flex: 1,
       margin: 5,
       flexDirection: 'row',
       justifyContent: 'space-between',
       borderColor: 'gray', 
       borderWidth: 1,
       borderRadius: 20
    },
    viewInputComPicker: {
        width: '70%',    
    },
    pickerComInput: {
        height: 48, 
        borderColor: 'gray',
        width: '30%', 
        color: '#FFFFFF'
    },
    viewRow: {
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 20
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: 'gray',
        width: '100%',
        borderRadius: 20
    },
    rowResultado: {
        flex: 1,
        flexDirection: 'column',
        padding: 5, 
        width: '100%'
    },
    text: {
        fontSize: 25,
        color: 'white'
    },
    titlePicker: {
        marginTop:-12,
        paddingLeft:5,
        paddingRight:5, 
        marginBottom: -8,
        backgroundColor: 'rgb(48,63,102)',
        marginRight: 'auto',
        marginLeft: 5,
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold',
        zIndex: 9,
        borderRadius: 50
    }
});
